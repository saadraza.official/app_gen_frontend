import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import AuthLayout from "./layouts/Auth";
import AdminLayout from "./layouts/Admin";

export default function App(): React.ReactElement {
    return (
        <>
            <Switch>
                <Route path="/auth" render={(props) => <AuthLayout {...props} />} />
                <Route path="/admin" render={(props) => <AdminLayout {...props} />} />
                <Redirect from="/" to="/auth/login" />
            </Switch>
        </>
    );
}
