import axios, {AxiosInstance} from "axios";
import qs from "qs";
import {loginService} from "../modules/authentication/services";

/**
 * parse error response
 * @param string messages
 */
const parseError = (messages): Promise<string> => {
    // error
    if (messages) {
        return Promise.reject(messages);
    } else {
        return Promise.reject("Something went wrong, Please try again");
    }
};

/**
 * parse response
 * @param {object|null} response
 */
const parseBody = (response) => {
    if (response.status === 200 || response.status === 201) {
        return response.data;
    } else {
        return parseError(response.data.message);
    }
};

const http = (baseUrl: string): AxiosInstance => {
    const instance = axios.create({
        baseURL: baseUrl,
        paramsSerializer: function (params) {
            return qs.stringify(params, {indices: false});
        },
        timeout: 10000
    });

    /*****************************Start Request Handler of Axios (interceptors)***************************************/
    instance.interceptors.request.use(
        (config) => {
            config.headers["App-ID"] = process.env.APP_ID;

            if (localStorage.getItem(process.env.CURRENT_USER_KEY)) {
                config.headers["Authorization"] = JSON.parse(
                    localStorage.getItem(process.env.CURRENT_USER_KEY)
                ).token;
            }
            return config;
        },
        (error) => {
            return Promise.reject(error);
        }
    );

    /*****************************End Request Handler of Axios (interceptors)***************************************/

    /*****************************Start Response Handler of Axios (interceptors)***********************************/
    instance.interceptors.response.use(
        (response) => {
            return parseBody(response);
        },
        async (error) => {
            if (error.response.status === 401) {
                const isRefreshProcessRunning = await loginService.getRefreshTokenProcess();
                if (isRefreshProcessRunning) {
                    loginService.logout();
                    loginService.setRefreshTokenProcess(false);
                    return;
                } else {
                    await loginService.refreshToken();
                    loginService.setRefreshTokenProcess(true);
                    window.location.reload();
                }
            }
            console.warn("Error status", error.response.status);
            if (error.response) {
                return Promise.reject(error.response.data);
            } else {
                return Promise.reject(error);
            }
        }
    );
    /*****************************End Response Handler of Axios (interceptors)***********************************/

    return instance;
};

export const httpHandler = http;
