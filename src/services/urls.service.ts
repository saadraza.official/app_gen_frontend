/**
 * Manage URLS of API's
 */

export const baseEndpoint = {
    baseEndPoint: process.env.BASE_ENDPOINT
};

const v1 = "api/v1";

export const apiUrls = {
    login: `${v1}/login`,
    register: `${v1}/signup`,
    profile: `${v1}/profile`,
    refreshToken: `${v1}/token/refresh`,
    logout: `${v1}/logout`
};
