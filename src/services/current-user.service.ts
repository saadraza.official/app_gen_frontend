/**
 * Fetch Current User detail
 */
import {UserDetail} from "../modules/authentication/interfaces";

/**
 * export function getCurrentUser
 * - Fetch Current User
 * @return  {UserDetail | null}
 */
export const getCurrentUser = (): UserDetail | null => {
    const user = JSON.parse(localStorage.getItem(process.env.CURRENT_USER_KEY));
    return user && user.userDetail ? user.userDetail : null;
};
