/**
 *  Auth Routs
 */
import Login from "../modules/authentication/views/LoginPage";
import Register from "../modules/authentication/views/RegisterPage";

interface Routes {
    path: string;
    layout: string;
    name: string;
    mini: string;
    component: unknown;
}

const routes: Routes[] = [
    {
        path: "/login",
        layout: "/auth",
        name: "Login Page",
        mini: "LP",
        component: Login
    },
    {
        path: "/signup",
        layout: "/auth",
        name: "Signup Page",
        mini: "LP",
        component: Register
    }
];
export default routes;
