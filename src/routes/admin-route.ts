/**
 *  Admin Routs
 */
import Dashboard from "../modules/dashboard/views/dashboard";

interface Routes {
    path: string;
    layout: string;
    name: string;
    component: unknown;
}

const routes: Routes[] = [
    {
        path: "/dashboard",
        layout: "/admin",
        name: "Dashboard",
        component: Dashboard
    }
];
export default routes;
