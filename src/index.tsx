import React, {Suspense} from "react";
import ReactDOM from "react-dom";
import {Provider} from "react-redux";
import {HashRouter} from "react-router-dom";
import {StoreConfig} from "../src/modules/ConfigStore";
import App from "./App";
import "../assets/scss/general.scss";

// Redux Store
const store = StoreConfig;

ReactDOM.render(
    <Provider store={store}>
        <HashRouter>
            <Suspense fallback="Loading...">
                <App />
            </Suspense>
        </HashRouter>
    </Provider>,

    document.getElementById("root")
);
