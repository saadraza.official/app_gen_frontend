import React from "react";
import {Switch, Route, Redirect} from "react-router-dom";
import routes from "../routes/admin-route";
import Navbar from "../modules/shared/Navbar/Navbar";
import logo from "../../assets/img/Jur-logo.png";

/**
 * function Admin
 * - include admin pages
 */
export default function Adimin(): React.ReactElement {
    /**
     * Create all Admin Routes
     *
     * @param adminRoutes
     * @returns Route | Redirect
     */
    const getRoutes = (adminRoutes): Route | Redirect => {
        const isAuth = checkAuthentication();
        return adminRoutes.map((prop, key) => {
            if (prop.collapse) {
                return getRoutes(prop.views);
            }
            if (prop.layout === "/admin") {
                return isAuth ? (
                    <Route
                        path={prop.layout + prop.path}
                        key={key}
                        render={(routeProps) => <prop.component {...routeProps} />}
                    />
                ) : (
                    <Redirect to="/auth/login" />
                );
            } else {
                return null;
            }
        });
    };

    /**
     * Check permission, if user has logged in (USER GUARD)
     * @returns boolean
     */
    const checkAuthentication = (): boolean => {
        const hasPermission = false;
        if (
            localStorage.getItem(process.env.CURRENT_USER_KEY) &&
            JSON.parse(localStorage.getItem(process.env.CURRENT_USER_KEY)).token
        ) {
            return true;
        }
        return hasPermission;
    };

    return (
        <>
            <Navbar logo={logo} />
            <Switch>
                {getRoutes(routes)}
                <Redirect from="*" to="/admin/dashboard" />
            </Switch>
        </>
    );
}
