import React from "react";
import {Switch, Route, Redirect} from "react-router-dom";
import routes from "../routes/auth-route";
import logo from "../../assets/img/Jur-logo.png";
import leftImg from "../../assets/img/left-graphic.png";
import CopyRightText from "../modules/shared/Footer/CopyRightText";

/**
 * function Auth
 * - include Authentication pages
 */
export default function Auth(): React.ReactElement {
    /**
     * Create all Auth Routes
     *
     * @param authRoutes
     * @returns Route | Redirect
     */
    const getRoutes = (authRoutes): Route | Redirect => {
        const isAuth = checkAuthentication();
        return authRoutes.map((prop, key) => {
            if (prop.collapse) {
                return getRoutes(prop.views);
            }
            if (prop.layout === "/auth") {
                return !isAuth ? (
                    <Route path={prop.layout + prop.path} component={prop.component} key={key} />
                ) : (
                    <Redirect to="/admin/dashboard" />
                );
            } else {
                return null;
            }
        });
    };

    /**
     * Check permission, if user has logged in then Redirect to dashboard (USER GUARD)
     * @returns boolean
     */
    const checkAuthentication = () => {
        const hasPermission = false;
        if (
            localStorage.getItem(process.env.CURRENT_USER_KEY) &&
            JSON.parse(localStorage.getItem(process.env.CURRENT_USER_KEY)).token
        ) {
            return true;
        }
        return hasPermission;
    };

    return (
        <>
            <section>
                <div className="left-div">
                    <img className="auth-logo" alt="logo" src={logo} />
                    <div className="h2-sider-banner">
                        <h2 className="h2">Become a modern arbitrator, Now</h2>
                    </div>
                    <img className="left-graphic" alt="left-graphic" src={leftImg} />
                </div>
                <div className="right-section">
                    <Switch>
                        {getRoutes(routes)}
                        <Redirect from="*" to="/auth/login" />
                    </Switch>
                    <div className="footer">
                        <CopyRightText />
                    </div>
                </div>
            </section>
        </>
    );
}
