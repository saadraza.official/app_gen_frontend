import React from "react";
import home from "assets/img/random-home.png";
import CopyRightText from "../../shared/Footer/CopyRightText";

export default function dashboard(): React.ReactElement {
    return (
        <section className="height-section">
            <div className="left-div">
                <div className="h2-home-side-banner">
                    <h2 className="h2">Let&apos;s start</h2>
                    <p>
                        Most of this information should already be present in your arbitration
                        agreement. In case it is not, you can refer to the FAQ section to find out.
                        The counter-party will also be asked to mention their preference. The final
                        decision will be amicably made later by the arbitrator. Download File{" "}
                    </p>
                </div>
            </div>
            <div className="right-section w-65">
                <div className="home-wrapper">
                    <img className="illustrator" src={home} />
                    <a className="btn btn-primary m-5">Let&apos;s Start</a>
                </div>
                <div className="footer">
                    <CopyRightText />
                </div>
            </div>
        </section>
    );
}
