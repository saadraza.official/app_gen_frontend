/**
 * Manage users Actions
 * -login action
 * -register action
 * -logout action
 */
import {userConstants} from "../constants";
import {loginService} from "../services";
import {UserDetail, UserResponse, RegisterPayloads, ErrorResponse} from "../interfaces";

/**
 * Interface UserReducer
 */
interface UserReducer {
    type: string;
    user?: UserDetail | string;
    error?: ErrorResponse | null;
}

const requestLogin = (user: {email: string}): UserReducer => {
    return {type: userConstants.LOGIN_REQUEST, user: user.email};
};

const successLogin = (user: UserDetail): UserReducer => {
    return {type: userConstants.LOGIN_SUCCESS, user};
};

const failureLogin = (error: ErrorResponse): UserReducer => {
    return {type: userConstants.LOGIN_FAILURE, error};
};

const requestRegister = (user: {email: string}): UserReducer => {
    return {type: userConstants.REGESTER_REQUEST, user: user.email};
};

const successRegister = (user: UserDetail): UserReducer => {
    return {type: userConstants.REGESTER_SUCCESS, user};
};

const failureRegister = (error: ErrorResponse): UserReducer => {
    return {type: userConstants.REGESTER_FAILURE, error};
};

/**
 * LoginAction to manage login feature
 */
export const LoginAction =
    (email: string, password: string) =>
    (dispatch: (arg0: UserReducer) => void): void => {
        const result = {email: email, password: password};
        dispatch(requestLogin({email}));
        loginService.login(result).then(
            (response) => {
                loginService.setCurrentUser(response);

                loginService.userDetail().then(
                    (user: UserResponse) => {
                        const currentUserDetail = JSON.parse(
                            localStorage.getItem(process.env.CURRENT_USER_KEY)
                        );
                        currentUserDetail.userDetail = user.data;

                        /** Set user detail along with token detail in Local storage */
                        localStorage.setItem(
                            process.env.CURRENT_USER_KEY,
                            JSON.stringify(currentUserDetail)
                        );
                        dispatch(successLogin(user.data));
                    },
                    (error) => {
                        dispatch(failureLogin(error));
                    }
                );
            },
            (error) => {
                dispatch(failureLogin(error));
            }
        );
    };

/**
 * RegisterAction to manage new user account process
 *
 */
export const RegisterAction =
    (payload: RegisterPayloads) =>
    (dispatch: (arg0: UserReducer) => void): void => {
        const email = payload.email;
        dispatch(requestRegister({email}));
        loginService.register(payload).then(
            (userDetail: UserResponse) => {
                dispatch(successRegister(userDetail.data));
            },
            (error) => {
                dispatch(failureRegister(error));
            }
        );
    };

export const LogoutAction = (): {type: string} => {
    loginService.logout();
    return {type: userConstants.LOGOUT};
};
