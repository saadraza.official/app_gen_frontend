import React, {useState, useEffect} from "react";
import {FormGroup as Group, FormControl as Control, Validators} from "react-reactive-form";
import {RegisterAction} from "../action";
import {useDispatch, useSelector} from "react-redux";
import {RootState} from "../../ConfigStore";
import "../styles/auth.scss";

export default function Register(props: {history: string[]}): React.ReactElement {
    const [registerForm, setRegisterForm] = useState(null);
    const [errorShow, setErrorShow] = useState(false);
    const [error, setError] = useState(null);
    const [passwordType, setPasswordType] = useState("password");
    const [confirmPasswordType, setConfirmPasswordType] = useState("password");

    const dispatch = useDispatch();
    const {authentication} = useSelector((state: RootState) => state);

    useEffect(() => {
        setRegisterForm(formInputs());
    }, []);

    useEffect(() => {
        console.log(authentication);
        if (authentication && authentication.registered) {
            props.history.push("/auth/login");
        }
        if (authentication && authentication.registeredError) {
            setError(authentication.registeredError);
        }
    }, [props, authentication]);

    const formInputs = (): Group => {
        return new Group({
            first_name: new Control(null, [Validators.required]),
            last_name: new Control(null, [Validators.required]),
            email: new Control(null, [
                Validators.required,
                Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$")
            ]),
            password: new Control(null, [Validators.required, Validators.minLength(6)]),
            confirm_password: new Control(null, [Validators.required, Validators.minLength(6)]),
            is_agreed: new Control(null, [Validators.required])
        });
    };

    const handleChange = (e: {target: {name: string | number; value: string | null}}) => {
        registerForm.controls[e.target.name].setValue(e.target.value);
    };

    const handleClick = () => {
        if (registerForm.valid && matchPassword) {
            const payload = {
                first_name: registerForm.controls.first_name.value,
                last_name: registerForm.controls.last_name.value,
                email: registerForm.controls.email.value,
                password: registerForm.controls.password.value
            };
            setErrorShow(false);
            dispatch(RegisterAction(payload));
        } else {
            setErrorShow(true);
        }
    };

    const matchPassword = (): boolean => {
        return (
            registerForm.controls.password.value === registerForm.controls.confirm_password.value
        );
    };

    const redirectSingIn = () => {
        props.history.push("/auth/login");
    };

    const showAndHidePassword = (type: string): void => {
        setPasswordType(type);
    };

    const showAndHideConfirmPassword = (type: string): void => {
        setConfirmPasswordType(type);
    };

    const displayError = (loginErrors) => {
        return (
            <div className="error-msg">
                {loginErrors ? loginErrors.message : ""}
                {loginErrors &&
                    loginErrors.data &&
                    loginErrors.data.map((err, key) => {
                        return (
                            <div key={key}>
                                {" "}
                                - {err.key} : {err.value}{" "}
                            </div>
                        );
                    })}
            </div>
        );
    };

    return (
        <>
            <div className="login-form">
                <p className="auth-p">
                    All Ready a Member ?{" "}
                    <a className="a-default cursor-pointer" onClick={redirectSingIn}>
                        Sign In
                    </a>
                </p>

                <div className="login-heading">
                    <h3>Sign Up</h3>
                    <p>Let&apos;s get started with Jur</p>
                    {displayError(error)}
                </div>
                <div className="form">
                    <div className="flex">
                        <div className="pr-5">
                            <label>
                                First Name<span className="red-color">*</span>
                            </label>

                            <input
                                className="input-control"
                                type="text"
                                id="fname"
                                required
                                name="first_name"
                                onChange={handleChange}
                            />
                            <h6 className="error-msg">
                                {" "}
                                {errorShow &&
                                registerForm.controls["first_name"].errors &&
                                registerForm.controls["first_name"].errors.required
                                    ? "First name is required."
                                    : ""}
                            </h6>
                        </div>
                        <div className="pl-5">
                            <label>
                                Last Name<span className="red-color">*</span>
                            </label>

                            <input
                                className="input-control"
                                type="text"
                                id="lname"
                                required
                                name="last_name"
                                onChange={handleChange}
                            />
                            <h6 className="error-msg">
                                {" "}
                                {errorShow &&
                                registerForm.controls["last_name"].errors &&
                                registerForm.controls["last_name"].errors.required
                                    ? "Last name is required."
                                    : ""}
                            </h6>
                        </div>
                    </div>
                    <div>
                        <label>
                            Email Address <span className="red-color">*</span>
                        </label>

                        <input
                            className="input-control"
                            type="email"
                            id="email"
                            required
                            name="email"
                            onChange={handleChange}
                        />
                        <h6 className="error-msg">
                            {" "}
                            {errorShow &&
                            registerForm.controls["email"].errors &&
                            registerForm.controls["email"].errors.required
                                ? "Email is required."
                                : ""}
                            {errorShow &&
                            registerForm.controls["email"].errors &&
                            registerForm.controls["email"].errors.pattern
                                ? "Email is incorrect."
                                : ""}
                        </h6>
                    </div>
                    <div className="password-div">
                        <label>
                            Password <span className="red-color">*</span>
                        </label>
                    </div>
                    <div className="password-div">
                        <input
                            type={passwordType}
                            className="input-control"
                            required
                            id="password"
                            name="password"
                            onChange={handleChange}
                        />
                        {passwordType == "password" && (
                            <span
                                id="togglePassword"
                                className="togglepasswordspan"
                                onClick={() => showAndHidePassword("text")}
                            >
                                Show
                            </span>
                        )}
                        {passwordType == "text" && (
                            <span
                                id="togglePassword"
                                className="togglepasswordspan"
                                onClick={() => showAndHidePassword("password")}
                            >
                                Hide
                            </span>
                        )}
                        <h6 className="error-msg">
                            {" "}
                            {errorShow &&
                            registerForm.controls["password"].errors &&
                            registerForm.controls["password"].errors.required
                                ? "Password is required."
                                : ""}
                            {errorShow &&
                            registerForm.controls["password"].value !==
                                registerForm.controls["confirm_password"].value
                                ? "Password does not match."
                                : ""}
                        </h6>
                    </div>

                    <div className="password-div">
                        <label>
                            Confirm Password <span className="red-color">*</span>
                        </label>
                    </div>
                    <div className="password-div">
                        <input
                            type={confirmPasswordType}
                            className="input-control"
                            required
                            id="confirmpassword"
                            name="confirm_password"
                            onChange={handleChange}
                        />
                        {confirmPasswordType == "password" && (
                            <span
                                id="togglePassword"
                                className="togglepasswordspan"
                                onClick={() => showAndHideConfirmPassword("text")}
                            >
                                Show
                            </span>
                        )}
                        {confirmPasswordType == "text" && (
                            <span
                                id="togglePassword"
                                className="togglepasswordspan"
                                onClick={() => showAndHideConfirmPassword("password")}
                            >
                                Hide
                            </span>
                        )}
                        <h6 className="error-msg">
                            {" "}
                            {errorShow &&
                            registerForm.controls["confirm_password"].errors &&
                            registerForm.controls["confirm_password"].errors.required
                                ? "Confirm password is required."
                                : ""}
                            {errorShow &&
                            registerForm.controls["password"].value !==
                                registerForm.controls["confirm_password"].value
                                ? "Password does not match."
                                : ""}
                        </h6>
                    </div>
                    <div className="checkbox-field">
                        <input
                            type="checkbox"
                            name="is_agreed"
                            id="isAgreed"
                            onChange={handleChange}
                        />{" "}
                        <label>
                            I agreed to the{" "}
                            <a className="a-default" href="#">
                                Terms
                            </a>{" "}
                            and{" "}
                            <a className="a-default" href="#">
                                Conditions.
                            </a>
                        </label>
                        <h6 className="error-msg">
                            {" "}
                            {errorShow &&
                            registerForm.controls["is_agreed"].errors &&
                            registerForm.controls["is_agreed"].errors.required
                                ? "Please agree all terms and condition."
                                : ""}
                        </h6>
                    </div>
                    <div className="input-block">
                        <button className="btn default-btn" onClick={handleClick}>
                            Sign Up
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
}
