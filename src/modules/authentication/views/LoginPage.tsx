/**
 * Login Page
 * @author Muhammad Raza
 */
import React, {useState, useEffect} from "react";
import "../styles/auth.scss";
import {FormGroup as Group, FormControl as Control, Validators} from "react-reactive-form";
import {useDispatch, useSelector} from "react-redux";
import {LoginAction} from "../action";
import {RootState} from "../../ConfigStore";

export default function Login(props: {history: string[]}): React.ReactElement {
    const [loginForm, setLoginForm] = useState(null);
    const [errorShow, setErrorShow] = useState(false);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);
    const [newuserCreated, setNewuserCreated] = useState(false);
    const [newuserDetail, setNewuserDetail] = useState({email: "", last_name: ""});
    const [passwordType, setPasswordType] = useState("password");

    //Fetching authentication detail from redux states
    const {authentication} = useSelector((state: RootState) => state);

    const dispatch = useDispatch();

    useEffect(() => {
        setLoginForm(formInputs());
    }, []);

    useEffect(() => {
        setNewuserCreated(false);
        setError(null);
        setNewuserDetail({email: "", last_name: ""});

        // if user successfully logged in then redirect to dashboard
        if (authentication && authentication.loggedIn) {
            props.history.push("/admin/dashboard");
        }

        // if user has error in response then show error
        if (authentication && authentication.error) {
            setError(authentication.error);
        }

        // if new user has been created
        if (authentication && authentication.registered) {
            setNewuserCreated(true);
            setNewuserDetail(authentication.user);
        }
    }, [props, authentication]);

    /**
     *  Create FORM GROUP from Reactive form
     * @returns Group
     */
    const formInputs = (): Group => {
        return new Group({
            email: new Control(null, [
                Validators.required,
                Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$")
            ]),
            password: new Control(null, [Validators.required]),
            isremember: new Control(false)
        });
    };

    const handleChange = (e: {target: {name: string | number; value: string | null}}) => {
        loginForm.controls[e.target.name].setValue(e.target.value);
    };

    /**
     * Trigger function on Login button
     */
    const handleClick = () => {
        if (loginForm.valid) {
            setErrorShow(false);
            setLoading(true);

            //Dispatch Login Action to perform login process
            dispatch(
                LoginAction(loginForm.controls.email.value, loginForm.controls.password.value)
            );
            setLoading(false);
        } else {
            setErrorShow(true);
        }
    };

    const showAndHidePassword = (type: string): void => {
        setPasswordType(type);
    };

    const redirectSingup = () => {
        props.history.push("/auth/signup");
    };

    const displayError = (loginErrors) => {
        return (
            <div className="error-msg">
                {loginErrors ? loginErrors.message : ""}
                {loginErrors &&
                    loginErrors.data &&
                    loginErrors.data.map((err, key) => {
                        return (
                            <div key={key}>
                                {" "}
                                - {err.key} : {err.value}{" "}
                            </div>
                        );
                    })}
            </div>
        );
    };

    return (
        <>
            <div className="login-form">
                <p className="auth-p">
                    {" "}
                    New to Jur ?{" "}
                    <a className="a-default cursor-pointer" onClick={redirectSingup}>
                        Sign Up
                    </a>
                </p>
                <div className="login-heading">
                    <h3>Sign In</h3>
                    <p>Let&apos;s get started with Jur</p>

                    {displayError(error)}

                    <div className="success-msg">
                        {newuserCreated
                            ? `Welcome ${newuserDetail.last_name}, you account has been created successfully, Please Login to continue `
                            : ""}
                    </div>
                </div>

                <div className="form">
                    <div>
                        <label>
                            Email Address<span className="red-color">*</span>
                        </label>

                        <input
                            className="input-control"
                            type="email"
                            id="email"
                            required
                            name="email"
                            onChange={handleChange}
                        />
                        <h6 className="error-msg">
                            {" "}
                            {errorShow &&
                            loginForm.controls["email"].errors &&
                            loginForm.controls["email"].errors.required
                                ? "Email is required."
                                : ""}
                            {errorShow &&
                            loginForm.controls["email"].errors &&
                            loginForm.controls["email"].errors.pattern
                                ? "Email is incorrect"
                                : ""}
                        </h6>
                    </div>
                    <div className="password-div">
                        <label>
                            Password <span className="red-color">*</span>
                        </label>

                        <a className="forget-password-link a-default" href="#">
                            Forget Password?
                        </a>
                    </div>
                    <div className="password-div">
                        <input
                            type={passwordType}
                            className="input-control"
                            required
                            id="password"
                            name="password"
                            onChange={handleChange}
                        />
                        {passwordType == "password" && (
                            <span
                                id="togglePassword"
                                className="togglepasswordspan"
                                onClick={() => showAndHidePassword("text")}
                            >
                                Show
                            </span>
                        )}
                        {passwordType == "text" && (
                            <span
                                id="togglePassword"
                                className="togglepasswordspan"
                                onClick={() => showAndHidePassword("password")}
                            >
                                Hide
                            </span>
                        )}

                        <h6 className="error-msg">
                            {" "}
                            {errorShow &&
                            loginForm.controls["password"].errors &&
                            loginForm.controls["password"].errors.required
                                ? "Password is incorrect"
                                : ""}
                        </h6>
                    </div>
                    <div className="checkbox-field">
                        <input
                            type="checkbox"
                            name="isremember"
                            id="rememberMe"
                            onChange={handleChange}
                        />{" "}
                        <label>Remember me</label>
                        <div>{errorShow}</div>
                    </div>
                    <div>
                        <button className="btn default-btn" onClick={handleClick}>
                            {" "}
                            {loading ? "Loading..." : "Sign In"}
                        </button>{" "}
                    </div>
                </div>
            </div>
        </>
    );
}
