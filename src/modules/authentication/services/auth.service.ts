import {httpHandler, apiUrls, baseEndpoint} from "../../../services";
import {AxiosInstance} from "axios";
import {BehaviorSubject} from "rxjs";
import {Login, CurrentUserDetail, UserDetail, UserResponse, RegisterPayloads} from "../interfaces";

class LoginService {
    protected http: AxiosInstance = null;
    constructor() {
        this.http = httpHandler(baseEndpoint.baseEndPoint);
    }

    subject = new BehaviorSubject(false);

    /**
     * Login User
     * @param {Promise<Login>} params
     */
    public login(params): Promise<Login> {
        return this.http.post(apiUrls.login, params);
    }

    /**
     * Register New User
     *
     * @param {RegisterPayloads<UserResponse>} params
     *
     * @return  {Promise<UserResponse>}
     */
    public register(params: RegisterPayloads): Promise<UserResponse> {
        return this.http.post(apiUrls.register, params);
    }

    public userDetail(): Promise<UserResponse> {
        return this.http.get(apiUrls.profile);
    }

    /**
     * Set Current User
     *
     * @param   {Login}       tokenDetail
     * @param   {UserDetail}  userDetail
     *
     * @return  {void}
     */
    public setCurrentUser(tokenDetail: Login, userDetail: UserDetail = null): void {
        localStorage.setItem(
            process.env.CURRENT_USER_KEY,
            JSON.stringify(this.normalizeTokenDetail(tokenDetail, userDetail))
        );
    }

    public getCurrentUser = () => {
        const user = JSON.parse(localStorage.getItem(process.env.CURRENT_USER_KEY));
        return user && user.userDetail ? user.userDetail : null;
    };

    /**
     * [normalizeTokenDetail description]
     *
     * @param  {Login}      tokenDetail
     * @param  {UserDetail} userDetail
     *
     * @return  {CurrentUserDetail}
     */
    private normalizeTokenDetail(tokenDetail: Login, userDetail: UserDetail): CurrentUserDetail {
        return {
            token: `${tokenDetail.token_type} ${tokenDetail.access_token}`,
            refreshToken: tokenDetail.refresh_token,
            userDetail: userDetail
        };
    }

    public setRefreshTokenProcess = (isRunning) => {
        this.subject.next(isRunning);
    };

    public getRefreshTokenProcess = async () => {
        let process = false;
        await this.subject.subscribe((resp) => {
            process = resp;
        });
        return process;
    };

    /**
     * Logout user
     */
    public logout = () => {
        localStorage.removeItem(process.env.CURRENT_USER_KEY);
        window.location.reload();
    };

    /**
     * Refresh Token if token get expired
     *
     * @return  {Login}
     */
    public refreshToken = async () => {
        const userDetail = JSON.parse(localStorage.getItem(process.env.CURRENT_USER_KEY));
        if (userDetail) {
            const params = {refresh_token: userDetail.refreshToken};
            const refreshToken: Login = await this.http.post(apiUrls.refreshToken, params);

            userDetail.token = `${refreshToken.token_type} ${refreshToken.access_token}`;
            userDetail.refreshToken = refreshToken.refresh_token;
            localStorage.setItem(process.env.CURRENT_USER_KEY, JSON.stringify(userDetail));
            return refreshToken;
        }
    };
}

export const loginService = new LoginService();
