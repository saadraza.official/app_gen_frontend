import {Login} from "./";

export interface AuthState {
    loggingIn?: boolean;
    loggedIn?: boolean;
    user?: Login | string;
    error?: string;
    registered?: boolean;
    registeredError?: string;
}
