export interface Login {
    token_type: string;
    expires_in: number;
    access_token: string;
    refresh_token: string;
}

export interface UserDetail {
    uuid: string;
    first_name: string;
    last_name: string;
    email: string;
}

export interface UserResponse {
    status: "success" | "error";
    message?: string | null;
    data: UserDetail | null;
}

export interface CurrentUserDetail {
    token: string;
    refreshToken: string;
    userDetail: UserDetail;
}
