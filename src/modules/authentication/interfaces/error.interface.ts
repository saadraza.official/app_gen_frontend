export interface ErrorResponse {
    status: "success" | "error";
    message?: string | null;
    data: ValidationError | null;
}

export interface ValidationError {
    key: string;
    value: string;
}
