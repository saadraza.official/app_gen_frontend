import {userConstants} from "../constants";
import {Login, AuthState} from "../interfaces";

const user = JSON.parse(localStorage.getItem(process.env.CURRENT_USER_KEY));

const initialState = user
    ? {loggedIn: true, user, error: null, registered: false, registeredError: null}
    : {};

interface UserAction {
    type: string;
    user: Login;
    error?: string;
}

/**
 * @param {[type]} state
 * @param   {UserAction}  action
 *
 * @return  {AuthState}
 */
export function authentication(state = initialState, action: UserAction): AuthState {
    switch (action.type) {
        case userConstants.LOGIN_REQUEST:
            return {
                loggingIn: true,
                user: action.user
            };
        case userConstants.LOGIN_SUCCESS:
            return {
                loggedIn: true,
                user: action.user
            };
        case userConstants.LOGIN_FAILURE:
            return {
                error: action.error
            };
        case userConstants.REGESTER_REQUEST:
            return {
                user: action.user
            };
        case userConstants.REGESTER_SUCCESS:
            return {
                registered: true,
                user: action.user
            };
        case userConstants.REGESTER_FAILURE:
            return {
                registeredError: action.error
            };
        case userConstants.LOGOUT:
            return {};
        default:
            return state;
    }
}
