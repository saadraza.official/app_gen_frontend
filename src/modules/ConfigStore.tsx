import {createStore, applyMiddleware, combineReducers} from "redux";
import thunk from "redux-thunk";
import {authentication} from "../modules/authentication/reducers";
/**
 * Configuration of store
 */
const rootReducer = combineReducers({
    authentication
});

export const StoreConfig = createStore(rootReducer, applyMiddleware(thunk));

export type RootState = ReturnType<typeof StoreConfig.getState>;
