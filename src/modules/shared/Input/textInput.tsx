import React, {useState} from "react";

interface TextInputProps {
    name?: string;
    value?: string;
    handleChange?: string[];
    onBlur?: {target: {value: string}};
    type?: string;
    placeholder?: string | null;
    label?: string;
    error?: string;
    required?: boolean;
}

export default function TextInput(props: TextInputProps): React.ReactElement {
    const {type, placeholder, label, error, required, name} = props;
    const [value, setValue] = useState(props.value);

    const handleChange = (e: {target: {value: string}}) => {
        setValue(e.target.value);
        if (props.handleChange) {
            console.log("value");
        }
    };

    return (
        <>
            <div>
                <label>
                    {label} <span className="red-color"> {required ? "*" : ""}</span>
                </label>

                <input
                    placeholder={placeholder}
                    className="input-control"
                    type={type}
                    required={required}
                    name={name}
                    value={value}
                    onChange={handleChange}
                />
                <h6 className="error-msg">{error}</h6>
            </div>
        </>
    );
}
