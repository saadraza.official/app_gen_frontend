import React from "react";
import {LogoutAction} from "../../authentication/action";
import {useDispatch} from "react-redux";
import {getCurrentUser} from "../../../services";

const Navbar = (props: {logo: string}): React.ReactElement => {
    const dispatch = useDispatch();
    const logout = (): void => {
        dispatch(LogoutAction);
    };

    return (
        <>
            <div className="nav">
                <input type="checkbox" id="nav-check" />
                <div className="nav-header">
                    <div className="nav-title">
                        <img alt="logo" className="brand-logo" src={props.logo} />
                    </div>
                </div>
                <div className="nav-btn">
                    <label>
                        <span></span>
                        <span></span>
                        <span></span>
                    </label>
                </div>

                <div className="nav-links">
                    <div className="nav-items">
                        <a className="cursor-pointer">Dashboard</a>
                    </div>
                    <div className="profile-link">
                        <span className="profile">
                            {getCurrentUser()?.first_name.charAt(0).toUpperCase() +
                                getCurrentUser()?.last_name.charAt(0).toUpperCase()}
                        </span>
                        <a className="cursor-pointer" onClick={logout}>
                            Logout
                        </a>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Navbar;
