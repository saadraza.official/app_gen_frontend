import React from "react";

const CopyRightText = (): React.ReactElement => {
    return (
        <>
            <p>Copyright &copy; 2021 Product by Jur inc.</p>
        </>
    );
};

export default CopyRightText;
