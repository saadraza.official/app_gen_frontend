FROM node:14-alpine

RUN mkdir -p /usr/src/app && chown -R node:node /usr/src/app

WORKDIR /usr/src/app

COPY package*.json ./

USER node

RUN npm install

COPY . .

EXPOSE 8080

COPY .env.example .env

CMD ["npm", "start"]